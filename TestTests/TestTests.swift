//
//  TestTests.swift
//  TestTests
//
//  Created by Jenkins on 12/18/18.
//  Copyright © 2018 Jenkins. All rights reserved.
//

import XCTest
@testable import Test

class TestTests: XCTestCase {

    var networkService: NetworkService?
    
    override func setUp() {
        networkService = NetworkService()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testNetworkService1() {
        let expectation = XCTestExpectation(description: "Fetch orders")
        
        networkService?.fetchOrders(value: 0, completion: { (result) in
            XCTAssertEqual(0, result?.keys.count)
            expectation.fulfill()
        })
        
        wait(for: [expectation], timeout: 20.0)
    }

    func testNetworkService2() {
        let expectation = XCTestExpectation(description: "Fetch orders")
        
        networkService?.fetchOrders(value: 4, completion: { (result) in
            XCTAssertNil(result)
            expectation.fulfill()
        })
        
        wait(for: [expectation], timeout: 20.0)
    }
    
    func testNetworkService3() {
        let expectation = XCTestExpectation(description: "Fetch orders")
        
        networkService?.fetchOrders(value: 4, completion: { (result) in
            XCTAssertEqual(4, result?["value"] as? Int)
            expectation.fulfill()
        })
        
        wait(for: [expectation], timeout: 20.0)
    }
}
