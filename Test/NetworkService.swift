//
//  NetworkService.swift
//  Test
//
//  Created by Jenkins on 12/19/18.
//  Copyright © 2018 Jenkins. All rights reserved.
//

import Foundation
class NetworkService {
    
    func fetchOrders(value: Int, completion: @escaping ([String: Any]?)->()) {
        if value < 0 {
            completion(nil)
        } else if value == 0 {
            completion([:])
        } else {
            completion(["value": value])
        }
    }
    
}
